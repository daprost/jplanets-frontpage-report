package de.jplanets.maven.report.frontpage;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * move to test sources!
 *
 */
public class Test {

	/**
	 * @param args test
	 */
	public static void main(String[] args) {
		// 1-1:2:1-1:2
		// 1[2]:1:1-1:2
		// 1[2](eins):2:2
		// split into rows
		//		parseFormat("1");
		//		parseFormat("1[1]");
		//		parseFormat("1(eins)");
		//		parseFormat("1[1](eins)");
		//		parseFormat("1-1:2:1-1:2");
		//		parseFormat("1[2]:1:1-1:2");
		//		parseFormat("1[2](eins):2:2");
		parseFormat("1{.id}");
		parseFormat("1{#class}");
		//		parseFormat("1[1]");
		//		parseFormat("1(eins)");
		parseFormat("1[1](eins){#class}");
		//		parseFormat("1-1:2:1-1:2");
		//		parseFormat("1[2]:1:1-1:2");
		//		parseFormat("1[2](eins):2:2");

	}

	//	public  static void parseFormat(SimpleRendererConfiguration src) {
	//		src.setRows(parseFormat(src.getFastSetup()));
	//		
	//	}
	/**
	 * @param simpleFormat test
	 */
	public static void parseFormat(String simpleFormat) {
		Pattern colPatter = Pattern.compile("(\\d*)?(?:\\[(\\d*)\\])?(?:\\((.*?)\\)?)?(?:\\{(.*?)\\})?");
		StringTokenizer rowTokenizer = new StringTokenizer(simpleFormat, ":");
		while (rowTokenizer.hasMoreTokens()) {
			String row = rowTokenizer.nextToken();
			System.out.println("ROW    :" + row);
			// split into cols
			StringTokenizer colTokenizer = new StringTokenizer(row, "-");
			while (colTokenizer.hasMoreTokens()) {
				String col = colTokenizer.nextToken();
				System.out.println("COL    :" + col);
				Matcher m = colPatter.matcher(col);
				if (m.matches()) {

				}
				String colspan = m.group(1);
				if (colspan == null) {
					colspan = "1";
				}
				System.out.println("COLSPAN:" + colspan);

				String rowspan = m.group(2);
				if (rowspan == null) {
					rowspan = "1";
				}
				System.out.println("ROWSPAN:" + rowspan);

				String conId = m.group(3);
				if (conId == null) {
					conId = "";
				}
				System.out.println("CON-ID :" + conId);
				String style = m.group(4);
				if (style == null) {
					style = "";
				}
				System.out.println("STYLE  :" + style);

			}
			System.out.println("--------");
		}
	}
}
