package de.jplanets.maven.report.frontpage;

import java.util.Map;

/**
 * Simple FrontpageManager using Maps.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 * @plexus.component
 */
public class DefaultFrontpageManager implements FrontpageManager {

	/**
	 * @plexus.requirement role="de.jplanets.maven.report.frontpage.ContentManager"
	 */
	@SuppressWarnings("rawtypes")
	private Map contentManagers;

	/**
	 * @plexus.requirement role="de.jplanets.maven.report.frontpage.LayoutManager"
	 */
	@SuppressWarnings({ "rawtypes" })
	private Map layoutManagers;

	/**
	 * {@inheritDoc}
	 */
	public ContentManager getContentManager(String id) {
		return (ContentManager) contentManagers.get(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public LayoutManager getLayoutManager(String id) {
		return (LayoutManager) layoutManagers.get(id);
	}

}
