package de.jplanets.maven.report.frontpage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.maven.doxia.Doxia;
import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import de.jplanets.maven.report.frontpage.model.FrontpageModel;
import de.jplanets.maven.report.frontpage.model.io.xpp3.FrontpageXpp3Reader;

/**
 * The FrontpageReport brings together a {@link ContentManager} with a {@link LayoutManager} to render the content as defined by the layout.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 * @goal frontpage-report
 * @phase site
 */
public class FrontpageReport extends AbstractMavenReport {

	// TODO avoid being displayed in the reports section?

	/**
	 * Directory where reports will go.
	 * 
	 * @parameter expression="${project.reporting.outputDirectory}"
	 * @required
	 */
	private String outputDirectory;

	/**
	 * The Project.
	 * 
	 * @parameter default-value="${project}"
	 * @required
	 * @readonly
	 */
	private MavenProject project;

	/**
	 * The renderer.
	 * 
	 * @component
	 * @required
	 * @readonly
	 */
	private Renderer siteRenderer;
	/**
	 * @component
	 * @required
	 * @readonly
	 */
	private Doxia doxia;
	/**
	 * @component
	 * @required
	 * @readonly
	 */
	private FrontpageManager frontpageManager;
	/**
	 * Name of the page description. Relative to /src/site.
	 * 
	 * @parameter default-value="src/site/frontpage.xml"
	 * @required
	 * @readonly
	 */
	private File model;

	// the models to use
	private Map<Locale, FrontpageModel> frontpageModels = new HashMap<Locale, FrontpageModel>();
	// the default locale
	private Locale defaultLocale = null;

	/**
	 * {@inheritDoc}
	 */
	protected void executeReport(Locale locale) throws MavenReportException {
		//getLog().warn("executeReport(" + locale + ")");
		putDefaultLocale(locale);
		loadModel(locale);
		FrontpageModel frontpageModel = frontpageModels.get(locale);
		ContentManager contentManager = frontpageManager.getContentManager(frontpageModel.getContent().getType());
		Xpp3Dom contentConfiguration = (Xpp3Dom) frontpageModel.getContent().getConfiguration();
		try {
			contentManager.setMavenProject(project);
			contentManager.configureXML(new StringReader(contentConfiguration.toString()));
		} catch (IOException e) {
			throw new MavenReportException("Could not configure the ContentManager", e);
		}

		LayoutManager layoutManager = frontpageManager.getLayoutManager(frontpageModel.getLayout().getType());
		Xpp3Dom layoutConfiguration = (Xpp3Dom) frontpageModel.getLayout().getConfiguration();
		try {
			layoutManager.setContentManager(contentManager);
			layoutManager.setDoxia(doxia);
			layoutManager.setSink(getSink());
			layoutManager.setMavenProject(project);
			layoutManager.configureXML(new StringReader(layoutConfiguration.toString()));
		} catch (IOException e) {
			throw new MavenReportException("Could not configure the LayoutManager", e);
		}

		try {
			if (locale.equals(defaultLocale)) {
				layoutManager.renderLayout(null);
			} else {
				layoutManager.renderLayout(locale);
			}
		} catch (IOException e) {
			throw new MavenReportException("Could not render the layout", e);
		}
	}

	private void loadModel(Locale locale) throws MavenReportException {
		putDefaultLocale(locale);
		if (frontpageModels.get(locale) != null) {
			return;
		}
		try {
			File modelFile = model;
			if (locale != null && !defaultLocale.equals(locale)) {
				modelFile = new File(model.getParent(), locale + File.separator + model.getName());
				if (!modelFile.exists()) {
					getLog().info("Using default model file for locale " + locale + ".");
					modelFile = model;
				}
			}
			FrontpageModel frontpageModel = new FrontpageXpp3Reader().read(new FileReader(modelFile));
			frontpageModels.put(locale, frontpageModel);
		} catch (FileNotFoundException e) {
			throw new MavenReportException("FileNotFoundException for '" + model.getAbsolutePath() + "'", e);
		} catch (IOException e) {
			throw new MavenReportException("IOException for '" + model.getAbsolutePath() + "'", e);
		} catch (XmlPullParserException e) {
			throw new MavenReportException("XmlPullParserException for '" + model.getAbsolutePath() + "'", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	protected String getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * {@inheritDoc}
	 */
	protected MavenProject getProject() {
		return project;
	}

	/**
	 * {@inheritDoc}
	 */
	protected Renderer getSiteRenderer() {
		return siteRenderer;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getDescription(Locale locale) {
		putDefaultLocale(locale);
		// TODO i18n
		// TODO configurable
		//getLog().warn("getDescription(" + locale + ")");
		return "The FrontPageReport";
	}

	/**
	 * {@inheritDoc}
	 */
	public String getName(Locale locale) {
		putDefaultLocale(locale);
		// TODO i18n
		// TODO configurable
		//getLog().warn("getName(" + locale + ")");
		return "FrontPageReport";
	}

	/**
	 * {@inheritDoc}
	 */
	public String getOutputName() {
		//getLog().warn("getOutputName()");
		try {
			loadModel(null);
		} catch (MavenReportException e) {
			getLog().warn("Loading model failed. Using 'frontpage' as output name", e);
			return "frontpage";
		}
		return frontpageModels.get(null).getOutputName();
	}

	private void putDefaultLocale(Locale locale) {
		// first locale to grab is the default.
		if (defaultLocale == null && locale != null) {
			defaultLocale = locale;
		}
	}
}
