package de.jplanets.maven.report.frontpage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Locale;

import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import de.jplanets.maven.report.frontpage.defaultcontent.model.DefaultContent;
import de.jplanets.maven.report.frontpage.defaultcontent.model.Item;
import de.jplanets.maven.report.frontpage.defaultcontent.model.io.xpp3.DefaultContentXpp3Reader;

/**
 * Implementation of a {@link ContentManager} which allows to use project (site) files or content directly written into the configuration.<br/>
 * <p>
 * <em>Usage of project (site) files:</em><br/>
 * If files are used, the <code>type</code> of the {@link Item} must not be defined, as it is determined by the file extenison.<br/>
 * Analogous to the common site handling the files must reside inside a directory named as the type (e.g.
 * ${project.basedir}/src/site/news/<b>apt</b>/news.<b>apt</b>). <br/>
 * The <code>location</code> of the {@link Item} is the relative path to the directory of the type (e.g. ${project.basedir}/<b>src/site/news</b>/apt/news.apt).
 * </p>
 * <p>
 * <em>Usage of direct content:</em><br/>
 * If direct content is used, the <code>type</code> of the {@link Item} must be defined. <code>fileName</code> and <code>location</code> will be ignored.
 * </p>
 * TODO link to xml configuration description.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 * @plexus.component role="de.jplanets.maven.report.frontpage.ContentManager" role-hint="default"
 */
public class DefaultContentManager implements ContentManager {

	/**
	 * The Project.
	 */
	private MavenProject project;

	/**
	 * The configuration.
	 */
	private DefaultContent defaultContent;
	/**
	 * Counter for nextContent().
	 */
	private int contentCount = 0;

	/**
	 * {@inheritDoc}
	 * <p>
	 * This implementation does not remove retrieved content from the iterating style.
	 * </p>
	 */
	public ContentItem contentById(String contentId, Locale locale) throws IOException {
		for (Item item : defaultContent.getItems()) {
			if (item.getId() != null && item.getId().equals(contentId)) {
				// TODO make configurable to remove content from iterating style
				return item2contentItem(item, locale);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * This implementation iterates over all content defined in the configuration.<br/>
	 * If all content is delivered, <code>null</code> is returned.
	 * </p>
	 */
	public ContentItem nextContent(Locale locale) throws IOException {
		if (contentCount >= defaultContent.getItems().size()) {
			// TODO make configurable if iterating restarts automatically
			return null;
		}

		Item item = defaultContent.getItems().get(contentCount++);
		return item2contentItem(item, locale);
	}

	private ContentItem item2contentItem(Item item, Locale locale) throws FileNotFoundException {
		String type = item.getType();
		if (type == null) {
			type = item.getFilename().substring(item.getFilename().lastIndexOf('.') + 1);
		}

		ContentItem contentItem = new ContentItem();
		contentItem.setContentType(type);
		if (item.getContent() == null || item.getContent().trim().length() == 0) {
			File contentFile = new File(project.getBasedir() + "/" + item.getLocation() + "/" + type + "/" + item.getFilename());
			if (locale != null) {
				File tryContent = new File(project.getBasedir() + "/" + item.getLocation() + "/" + locale + "/" + type + "/" + item.getFilename());
				if (tryContent.canRead()) {
					contentFile = tryContent;
				}
			}
			contentItem.setContent(new FileReader(contentFile));
		} else {
			contentItem.setContent(new StringReader(item.getContent()));
		}
		return contentItem;
	}

	/**
	 * {@inheritDoc}
	 */
	public void reset() {
		contentCount = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	public void configureXML(Reader reader) throws IOException {
		try {
			defaultContent = new DefaultContentXpp3Reader().read(reader);
			reset();
		} catch (XmlPullParserException e) {
			throw new IOException("Exception parsing configuration.", e);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	public final void setMavenProject(MavenProject mavenProject) {
		this.project = mavenProject;
	}

	/**
	 * @return the defaultContent
	 */
	public final DefaultContent getDefaultContent() {
		return defaultContent;
	}

	/**
	 * @param defaultContent the defaultContent to set
	 */
	public final void setDefaultContent(DefaultContent defaultContent) {
		this.defaultContent = defaultContent;
	}

}
