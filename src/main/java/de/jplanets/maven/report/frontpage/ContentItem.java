package de.jplanets.maven.report.frontpage;

import java.io.Reader;

/**
 * Wrapper for content to render. The content type determins the parser to use.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 */
public class ContentItem {

	// TODO i18n
	private Reader content;
	private String contentType;

	/**
	 * Get the content to be rendered.
	 * 
	 * @return a Reader for the Sink
	 */
	public Reader getContent() {
		return content;
	}

	/**
	 * Set the content to be rendered.
	 * 
	 * @param content the Reader for the Sink
	 */
	public void setContent(Reader content) {
		this.content = content;
	}

	/**
	 * Get the content type.
	 * 
	 * @return the content type to determin the parser to use.
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Set the content type.
	 * 
	 * @param contentType the content type to determin the parser to use.
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
