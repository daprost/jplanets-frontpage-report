package de.jplanets.maven.report.frontpage;

import java.io.IOException;
import java.io.Reader;
import java.util.Locale;

import org.apache.maven.doxia.Doxia;
import org.apache.maven.doxia.parser.Parser;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.MavenReportException;

/**
 * A LayoutManager renders the content provided by a {@link ContentManager}.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 */
public interface LayoutManager {

	/** The Plexus lookup role. */
	String ROLE = LayoutManager.class.getName();

	/**
	 * Render the layout provided by the {@link ContentManager} using the given {@link Sink}.
	 * 
	 * @param locale the wanted locale to generate the report, could be null.
	 * @throws IOException If rendering fails due to missing content.
	 * @throws MavenReportException If rendering fails due to invalid report stuff.
	 */
	public void renderLayout(Locale locale) throws IOException, MavenReportException;

	/**
	 * Configure the LayoutManager providing xml configuration.
	 * 
	 * @param reader Reader conatining xml configuration.
	 * @throws IOException if reading configuration fails.
	 */
	public void configureXML(Reader reader) throws IOException;

	/**
	 * @param doxia Doxia to support different content {@link Parser Parsers}.
	 */
	public void setDoxia(Doxia doxia);

	/**
	 * @param sink The {@link Sink} for the content.
	 */
	public void setSink(Sink sink);

	/**
	 * @param contentManager The {@link ContentManager} to provide the content.
	 */
	public void setContentManager(ContentManager contentManager);

	/**
	 * @param mavenProject The maven project using the LayoutManager.
	 */
	public void setMavenProject(MavenProject mavenProject);

}
