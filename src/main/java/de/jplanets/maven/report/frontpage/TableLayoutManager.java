package de.jplanets.maven.report.frontpage;

import java.io.IOException;
import java.io.Reader;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.doxia.Doxia;
import org.apache.maven.doxia.parser.ParseException;
import org.apache.maven.doxia.parser.Parser;
import org.apache.maven.doxia.parser.manager.ParserNotFoundException;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.SinkEventAttributeSet;
import org.apache.maven.doxia.sink.SinkEventAttributes;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.MavenReportException;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import de.jplanets.maven.report.frontpage.tablelayout.model.TableColumn;
import de.jplanets.maven.report.frontpage.tablelayout.model.TableLayout;
import de.jplanets.maven.report.frontpage.tablelayout.model.TableRow;
import de.jplanets.maven.report.frontpage.tablelayout.model.io.xpp3.TablelayoutXpp3Reader;

/**
 * <p>
 * The TableLayoutManager renders the content as a html table.
 * </p>
 * <p>
 * <em><strong>The fast setup format</strong></em><br/>
 * <table>
 * <tr>
 * <td valign='top' align='right'><em>FastSetup</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'><em>Row</em> | <em>Row</em> ( ':' <em>Row</em> )*</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>The fast setup format contains of colon separated row definitions.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>Row</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'><em>Col</em> | <em>Col</em> ( '-' <em>Col</em> )*</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>Each row definition contains of minus searated column definitions.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>Col</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'><em>ColSpan</em>? <em>RowSpan</em>? <em>ContentId</em>? <em>Style</em>?</em></td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>Each column definition contains of the colspan, rowspan, contentid and style definition.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>ColSpan</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'>\d*</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>The colspan contains the number of columns to span. Defaults to 1.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>RowSpan</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'>'[' \d* ']'</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>The rowspan contains the number of rows to span sourrounded by square brackets. Defaults to [1].</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>ContentId</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'>'(' \d* ')'</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>The contentid contains the reference to conent sourrounded by braces. Defaults to () which means no content reference is used.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>Style</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'>'{' <em>CellClass</em> | <em>CellId</em> '}'</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>The Style contains either a cellclass or a cellid.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>CellId</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'>'#' \w*</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>A cell id will be used as id-attribute in the table cell.</td>
 * </tr>
 * <tr>
 * <td valign='top' align='right'><em>CellClass</em></td>
 * <td valign='top' >::=</td>
 * <td valign='top'>'.' \w*</td>
 * <td>&nbsp;&nbsp;</td>
 * <td valign='top'>A cell class will be used as class-attribute in the table cell.</td>
 * </tr>
 * </table>
 * Example (omitting content): Fastsetup "<tt>2-1[3](news){.newstable}:2:1-1</tt>" will create
 * 
 * <pre>
 * &lt;table&gt;
 *   &lt;tr&gt;
 *     &lt;td colspan='2'&gt;&lt;/td&gt;
 *     &lt;td rowspan='3' class='newstable'&gt;[news]&lt;/td&gt;
 *   &lt;/tr&gt;
 *   &lt;tr&gt;
 *     &lt;td colspan='2'&gt;&lt;/td&gt;
 *   &lt;/tr&gt;
 *   &lt;tr&gt;
 *     &lt;td&gt;&lt;/td&gt;
 *     &lt;td&gt;&lt;/td&gt;
 *   &lt;/tr&gt;
 * &lt;/table&gt;
 * </pre>
 * 
 * </p>
 * TODO link to configuration description.<br/>
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 * @plexus.component role="de.jplanets.maven.report.frontpage.LayoutManager" role-hint="table"
 */
public class TableLayoutManager implements LayoutManager {

	private TableLayout tableLayout;
	private Sink sink;
	private Doxia doxia;
	private ContentManager contentManager;

	/**
	 * {@inheritDoc}
	 */
	public void renderLayout(Locale locale) throws IOException, MavenReportException {
		SinkEventAttributeSet ratts = new SinkEventAttributeSet();
		// TODO document class
		// TODO allow css-styles
		ratts.addAttribute(SinkEventAttributes.CLASS, "tableLayout");

		sink.table();
		// default justification, no border
		sink.tableRows(null, false);
		for (TableRow row : tableLayout.getRows()) {
			sink.tableRow(ratts);
			for (TableColumn col : row.getCols()) {
				renderCell(col, locale);
			}
			sink.tableRow_();
		}
		sink.table_();

	}

	/**
	 * render a single table cell.
	 * 
	 * @param sink
	 * @param doxia
	 * @param contentManager
	 * @param col
	 * @throws IOException
	 * @throws MavenReportException
	 */
	private void renderCell(TableColumn col, Locale locale) throws IOException, MavenReportException {
		SinkEventAttributeSet atts = new SinkEventAttributeSet();
		if (col.getColspan() > 1) {
			atts.addAttribute(SinkEventAttributes.COLSPAN, col.getColspan());
		}
		if (col.getRowspan() > 1) {
			atts.addAttribute(SinkEventAttributes.ROWSPAN, col.getRowspan());
		}
		if (col.getCellClass() != null && col.getCellClass().length() > 0) {
			atts.addAttribute(SinkEventAttributes.CLASS, col.getCellClass());
		}
		if (col.getCellId() != null && col.getCellId().length() > 0) {
			atts.addAttribute(SinkEventAttributes.ID, col.getCellId());
		}

		sink.tableCell(atts);
		renderContent(col, locale);
		sink.tableCell_();
	}

	/**
	 * render the content of a table cell
	 * 
	 * @param sink
	 * @param doxia
	 * @param contentManager
	 * @param col
	 * @throws IOException
	 * @throws MavenReportException
	 */
	private void renderContent(TableColumn col, Locale locale) throws IOException, MavenReportException {
		ContentItem cItem = null;
		if (col.getContentId() != null && col.getContentId().length() > 0) {
			cItem = contentManager.contentById(col.getContentId(), locale);
		} else {
			cItem = contentManager.nextContent(locale);
		}
		if (cItem != null) {
			try {
				Parser parser = doxia.getParser(cItem.getContentType());
				parser.parse(cItem.getContent(), sink);

			} catch (ParserNotFoundException e) {
				throw new MavenReportException("No parser of type '" + cItem.getContentType() + "' for table layout.", e);
			} catch (ParseException e) {
				throw new MavenReportException("Error parsing content for table layout.", e);
			}

		} else {
			// TODO document default content
			sink.nonBreakingSpace();
		}
	}

	/**
	 * convert the "fast-setup"-format into rows and cols.
	 */
	private void parseFastSetup() {
		String simpleFormat = tableLayout.getFastSetup();
		Pattern colPatter = Pattern.compile("(\\d*)?(?:\\[(\\d*)\\])?(?:\\((.*?)\\)?)?(?:\\{(.*?)\\})?");
		StringTokenizer rowTokenizer = new StringTokenizer(simpleFormat, ":");
		while (rowTokenizer.hasMoreTokens()) {
			TableRow tr = new TableRow();
			tableLayout.addRow(tr);
			String row = rowTokenizer.nextToken();
			// TODO validate cols in row
			StringTokenizer colTokenizer = new StringTokenizer(row, "-");
			while (colTokenizer.hasMoreTokens()) {
				String col = colTokenizer.nextToken();
				TableColumn td = new TableColumn();
				tr.addCol(td);
				Matcher m = colPatter.matcher(col);
				if (m.matches()) {
					String colspan = m.group(1);
					if (colspan == null) {
						colspan = "1";
					}
					td.setColspan(Integer.parseInt(colspan));

					String rowspan = m.group(2);
					if (rowspan == null) {
						rowspan = "1";
					}
					td.setRowspan(Integer.parseInt(rowspan));

					String conId = m.group(3);
					if (conId == null) {
						conId = "";
					}
					td.setContentId(conId);

					String style = m.group(4);
					if (style == null || style.length() <= 1) {
						td.setCellClass("");
						td.setCellId("");
					} else {
						if (style.startsWith(".")) {
							td.setCellClass(style.substring(1));
							td.setCellId("");
						} else if (style.startsWith("#")) {
							td.setCellClass("");
							td.setCellId(style.substring(1));
						} else {
							td.setCellClass("");
							td.setCellId("");
						}
					}
				}
			}
		}

	}

	/**
	 * Configure the layout using the provided xml.
	 */
	public void configureXML(Reader reader) throws IOException {
		try {
			tableLayout = new TablelayoutXpp3Reader().read(reader);
			if (tableLayout.getFastSetup() != null) {
				parseFastSetup();
			}
		} catch (XmlPullParserException e) {
			throw new IOException("Exception parsing configuration.", e);
		}
	}

	/**
	 * Configure the layout using provided 'FastSetup'.
	 * 
	 * @param fastSetup The setup string to use.
	 */
	public void configureFastSetup(String fastSetup) {
		tableLayout = new TableLayout();
		tableLayout.setFastSetup(fastSetup);
		parseFastSetup();
	}

	/**
	 * @return the tableLayout
	 */
	public final TableLayout getTableLayout() {
		return tableLayout;
	}

	/**
	 * Use this or one of the configure...-methods.
	 * 
	 * @param tableLayout the tableLayout to set
	 */
	public final void setTableLayout(TableLayout tableLayout) {
		this.tableLayout = tableLayout;
	}

	/**
	 * @return the sink
	 */
	public final Sink getSink() {
		return sink;
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setSink(Sink sink) {
		this.sink = sink;
	}

	/**
	 * @return the doxia
	 */
	public final Doxia getDoxia() {
		return doxia;
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setDoxia(Doxia doxia) {
		this.doxia = doxia;
	}

	/**
	 * @return the contentManager
	 */
	public final ContentManager getContentManager() {
		return contentManager;
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setContentManager(ContentManager contentManager) {
		this.contentManager = contentManager;
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setMavenProject(MavenProject mavenProject) {
		// nothing to do since MavenProject is not used.
	}

}
