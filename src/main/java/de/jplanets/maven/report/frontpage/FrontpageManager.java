package de.jplanets.maven.report.frontpage;

/**
 * The FrontpageManager manages the available {@link ContentManager ContentManagers} and {@link LayoutManager LayoutManagers}.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 */
public interface FrontpageManager {

	/** The Plexus lookup role. */
	String ROLE = FrontpageManager.class.getName();

	/**
	 * Get a ContentManager.
	 * 
	 * @param id Id of the ContenManager.
	 * @return A ContentManager or <code>null</code> if none is registered with the given id.
	 */
	public ContentManager getContentManager(String id);

	/**
	 * Get a LayoutManager.
	 * 
	 * @param id Id of the LayoutManager.
	 * @return A LayoutManager or <code>null</code> if none is registered with the given id.
	 */
	public LayoutManager getLayoutManager(String id);
}
