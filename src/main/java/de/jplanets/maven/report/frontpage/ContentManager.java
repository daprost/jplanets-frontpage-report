package de.jplanets.maven.report.frontpage;

import java.io.IOException;
import java.io.Reader;
import java.util.Locale;

import org.apache.maven.project.MavenProject;

/**
 * <p>
 * A ContentManager handles the content for a Frontpage and is used by a {@link LayoutManager}.<br/>
 * There are two ways to access the managed contents. Direct access by named content id and in an iterating style by calling
 * {@link ContentManager#nextContent(Locale)}.
 * </p>
 * <p>
 * The methods can throw an {@link IllegalStateException} if the ContentManager is not yet configured.
 * </p>
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 */
public interface ContentManager {

	/**
	 * Plexus stuff
	 */
	public static final String ROLE = ContentManager.class.getName();

	/**
	 * Get content by named id. <br/>
	 * Returns <code>null</code> if no content is available for the specified id.<br/>
	 * Returns the same content each time this method is called with the same id.<br/>
	 * Implementations may remove a retrieved content from the iterating style.
	 * 
	 * @param contentId the contens id.
	 * @param locale the locale to use.
	 * @return the content for the id.
	 * @throws IOException if reading content fails.
	 * @throws IllegalStateException if the ContentManager is not configured.
	 */
	public ContentItem contentById(String contentId, Locale locale) throws IOException;

	/**
	 * Get next content by some 'order'.<br/>
	 * Returns <code>null</code> if no next content is available.<br/>
	 * What an 'order' is is not determined by this interface. See implementation for details.<br/>
	 * When no next content is available is not determined by this interface. See implementation for details.<br/>
	 * 
	 * @param locale The locale to use. 
	 * @return The next content.
	 * @throws IOException If reading content fails.
	 * @throws IllegalStateException If the ContentManager is not configured.
	 */
	public ContentItem nextContent(Locale locale) throws IOException;

	/**
	 * Reset the 'order' of the content iteration to start iteration over again.<br/>
	 * 
	 * @throws IllegalStateException If the ContentManager is not configured.
	 */
	public void reset();

	/**
	 * Configure the ContentManager.
	 * 
	 * @param reader Reader conatining xml configuration.
	 * @throws IOException If reading configuration fails.
	 */
	public void configureXML(Reader reader) throws IOException;

	/**
	 * @param mavenProject The maven project using the LayoutManager.
	 */
	public void setMavenProject(MavenProject mavenProject);

}
